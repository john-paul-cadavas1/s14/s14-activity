console.log("Heelo World");

let firstName = "John";
console.log("First Name: " + firstName);

let lastName = "Smith";
console.log("Last Name: " + lastName);

let age = 38;
console.log("Age: " + age);

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);

console.log("Work Address: ")

let workAddress = {
	houseNumber: 32,
	street: "Washington", 
	city: "Lincon", 
	state: "Nebraska"
};
console.log(workAddress);

function fullName() {
	return firstName + " " + lastName + 'is' + ' ' + age + ' ' + 'years of age.'
}
console.log(fullName());
console.log("This was printed using a function");

function returnHobbies() {
	return hobbies;
}
console.log(returnHobbies());
console.log("This was printed using a function");

function returnWorkAddress() {
	return workAddress;
}

console.log(returnWorkAddress());